import  {v4 as uuid} from 'uuid';  //use uuid version 4
import AWS from 'aws-sdk';  //contains many functions to deal w/ AWS Services
import commonMiddleware from "../lib/commonMiddleware";
import createHttpError from 'http-errors';
import validator from "@middy/validator";
import createAuctionsSchema from "../lib/schemas/createAuctionsSchema";

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function createAuction(event, context) {
  const {title} = event.body;
  const { email } = event.requestContext.authorizer;
  const now = new Date();
  const endDate = new Date();
  endDate.setHours(now.getHours() + 1);  //Auctions expire after 1 hour.

  const auction = {
    id: uuid(),
    title,  //i.e. title = title;
    status: 'OPEN',
    createdAt: now.toISOString(),  //standard way to store dates in DB
    endingAt: endDate.toISOString(),
    highestBid: {
      amount: 0
    },
    seller: email,
  };

  try {
    await dynamodb.put({  //await makes it synchronous
      TableName: process.env.AUCTIONS_TABLE_NAME,
      Item: auction
    }).promise();
  }catch(error){
    console.error(error);
    throw new createHttpError.InternalServerError(error);  //create error in a decorative, clean way
  }

  return {
    statusCode: 201,  //stand for resource created
    body: JSON.stringify(auction),
  };
}

export const handler = commonMiddleware(createAuction)
    .use(validator({inputSchema: createAuctionsSchema}));
