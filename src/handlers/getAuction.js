import AWS from 'aws-sdk';  //contains many functions to deal w/ AWS Services
import commonMiddleware from "../lib/commonMiddleware";
import createHttpError from 'http-errors';

const dynamodb = new AWS.DynamoDB.DocumentClient();

export async function getAuctionById(id) {
    let auction;
    try {
        const result = await dynamodb.get({
            TableName: process.env.AUCTIONS_TABLE_NAME,
            Key: { id }  //i.e. Key: { id: id }
        }).promise();

        auction = result.Item;
    } catch (error) {
        console.error(error);
        throw new createHttpError.InternalServerError(error);
    }

    if (!auction) {
        throw new createHttpError.NotFound(`Auction with ID "${id}" not found!`); //use ` not '
    }

    return auction;
}

async function getAuction(event, context) {
    const { id } = event.pathParameters;
    const auction = await getAuctionById(id);

    return {
        statusCode: 200,  //stand for resource created
        body: JSON.stringify(auction),
    };
}

export const handler = commonMiddleware(getAuction);