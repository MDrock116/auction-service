import AWS from 'aws-sdk';  //contains many functions to deal w/ AWS Services
import commonMiddleware from "../lib/commonMiddleware";
import createHttpError from 'http-errors';
import validator from "@middy/validator";
import getAuctionsSchema from "../lib/schemas/getAuctionsSchema";

const dynamodb = new AWS.DynamoDB.DocumentClient();

async function getAuctions(event, context) {
    const { status } = event.queryStringParameters;
    let auctions;

    const params = {
        TableName: process.env.AUCTIONS_TABLE_NAME,
        IndexName: 'statusAndEndDate',
        KeyConditionExpression: '#status = :status',
        ExpressionAttributeValues: {
            ':status': status,
        },
        ExpressionAttributeNames: {
            '#status': 'status',  //'status' is a reserved word so '#status' was used in express instead & needs to be replaced w/ 'status' at runtime to match actual attribute name in table
        },
    };

    try {
        const result = await dynamodb.query(params).promise();

      auctions = result.Items;
    } catch (error) {
        console.error(error);
        throw new createHttpError.InternalServerError(error);
    }

    return {
        statusCode: 200,  //stand for resource created
        body: JSON.stringify(auctions),
    };
}

export const handler = commonMiddleware(getAuctions)
    .use(validator({inputSchema: getAuctionsSchema, useDefaults: true}));