import middy from "@middy/core";
import httpErrorHandler from "@middy/http-error-handler";
import validator from "@middy/validator";
import createHttpError from "http-errors";
import {getAuctionById} from "./getAuction";
import {uploadAuctionToS3} from "../lib/uploadPicutreToS3";
import {setAuctionPictureUrl} from "../lib/setAuctionPictureUrl";
import uploadAuctionPictureSchema from "../lib/schemas/uploadAuctionPictureSchema";
import cors from "@middy/http-cors";

export async function uploadAuctionPicture(event) {
    const { id } = event.pathParameters;
    const { email } = event.requestContext.authorizer;
    const auction = await getAuctionById(id);

    //Validate auction ownership
    if(auction.seller!==email){
        throw new createHttpError.Forbidden('You are not the seller of this auction');
    }

    const base64 = event.body.replace(/^data:image\/\w+;base64,/, '');  //store picture in variable
    const buffer = Buffer.from(base64, 'base64');  //buffer is used to upload pic to S3

    let updatedAuction;

    try{
        const  pictureURL = await uploadAuctionToS3(auction.id + '.jpg', buffer);
        console.log("The URL of the Picture is: " + pictureURL);
        updatedAuction = await setAuctionPictureUrl(auction.id, pictureURL);

    } catch (error) {
        console.error(error);
        throw new createHttpError.InternalServerError(error);
    }

    return {
        statusCode: 200,
        body: JSON.stringify({updatedAuction}),
    };
}

export const handler = middy(uploadAuctionPicture)
    .use(httpErrorHandler())
    .use(validator({inputSchema: uploadAuctionPictureSchema}))
    .use(cors());