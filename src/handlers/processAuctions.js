import {getEndedAuctions} from "../lib/getEndedAuctions";
import {closeAuction} from "../lib/closeAuction";
import createHttpError from "http-errors";

async function processAuctions(event, context) {
    try {
        const auctionToClose = await getEndedAuctions();
        const closePromises = auctionToClose.map(auction => closeAuction(auction));  //collect all promises
        await Promise.all(closePromises);  //run them all at the same time to get result
        return {closed: closePromises.length };
    } catch (error) {
        console.error(error);
        throw new createHttpError.InternalServerError(error);
    }
}

export const handler = processAuctions;