import middy from '@middy/core';
import httpJsonBodyParser from '@middy/http-json-body-parser';
import httpEventNormalizer from '@middy/http-event-normalizer';
import httpErrorHandler from "@middy/http-error-handler";
import cors from "@middy/http-cors";

export default handler => middy(handler)
    .use([
        httpJsonBodyParser(),   //parses all incoming JSON code
        httpEventNormalizer(),  //automatically adjusts API Gateway events to prevent errors from non-existing objects & reduces if statements
        httpErrorHandler(),      //makes error handling smooth & clean
        cors(),  //permits Cross Origin Resource Sharing to allow UI app to work
    ]);