import AWS from 'aws-sdk';

const dynamodb = new AWS.DynamoDB.DocumentClient();
const sqs = new AWS.SQS();

export async function closeAuction(auction) {
    const params = {
        TableName: process.env.AUCTIONS_TABLE_NAME,
        Key: { id: auction.id },
        UpdateExpression: 'set #status = :status',
        ExpressionAttributeValues: {
            ':status': 'CLOSED',
        },
        ExpressionAttributeNames: {
            '#status': 'status',  //'status' is a reserved word so '#status' was used in express instead & needs to be replaced w/ 'status' at runtime to match actual attribute name in table
        }
    };

    await dynamodb.update(params).promise();

    const { title, seller, highestBid } = auction;
    const { amount, bidder } = highestBid;

    if (amount === 0) {
        await  sqs.sendMessage({
            QueueUrl: process.env.MAIL_QUEUE_URL,
            MessageBody: JSON.stringify({
                subject: 'No bids on your auction item :(',
                recipient: seller,
                body: `Sorry you item "${title} didn't get any bids.  Better luck next time",`
            }),
        }).promise();
        return;
    }

    const notifySeller = sqs.sendMessage({
        QueueUrl: process.env.MAIL_QUEUE_URL,
        MessageBody: JSON.stringify({
           subject: 'Your item has been sold',
           recipient: seller,
           body: `Congrats! Your item "${title}", has been sold for $${amount}.`
        }),
    }).promise();

    const notifyBidder = sqs.sendMessage({
        QueueUrl: process.env.MAIL_QUEUE_URL,
        MessageBody: JSON.stringify({
            subject: 'You won an auction',
            recipient: bidder,
            body: `Way to go! You got yourself a "${title}" for $${amount}.`,
        }),
    }).promise();
    return Promise.all([notifySeller, notifyBidder]);
}