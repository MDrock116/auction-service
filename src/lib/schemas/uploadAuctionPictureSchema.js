const schema = {
    properties: {
        body: {
            type: 'string',
            minLength: 1,
            pattern: '\=$' //requires string to end with an equal sign, which is what a base64 picture must have
        },
    },
    required: ['body'],
};

export default schema;