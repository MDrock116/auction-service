import AWS from 'aws-sdk';  //contains many functions to deal w/ AWS Services

const dynamodb = new AWS.DynamoDB.DocumentClient();

export async function setAuctionPictureUrl(id, picUrl) {
    const params = {
        TableName: process.env.AUCTIONS_TABLE_NAME,
        Key: { id },
        UpdateExpression: 'set picURL =:picUrl',
        ExpressionAttributeValues: {
            ':picUrl': picUrl,
        },
        ReturnValues: 'ALL_NEW',
    };

    const result = await dynamodb.update(params).promise();
    return result.Attributes;  //returns all attributes of Auction just updated

}